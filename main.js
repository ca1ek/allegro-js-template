function update() {

}

function draw() {
    textout(canvas,font,"Hello World!",40,40,24,makecol(255,255,255));
}

function main() {
    enable_debug('debug');
    allegro_init_all("game_canvas", 640, 480);
    ready(() => {
        
        loop(() => {

            clear_to_color(canvas,makecol(0,0,0));
            update();
            draw();

        },BPS_TO_TIMER(60));

    });
    return 0;
}

END_OF_MAIN();
